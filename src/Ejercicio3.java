import java.util.Scanner;

public class Ejercicio3 {
	public static void main(String []args) {
		//entrada
		System.out.println("Ingrese la division de su curso (p.ej B)");
		Scanner s1 = new Scanner(System.in);
		char d = s1.next().charAt(0);
		System.out.println("Ingrese la cantidad de goles del partido");
		Scanner s2 =new Scanner(System.in);
		byte g=s2.nextByte();
		System.out.println("Ingrese la capacidad de personas de la cancha");
		Scanner s3=new Scanner(System.in);
		int c=s3.nextInt();
		System.out.println("Ingrese el promedio de los goles");
		Scanner s4 =new Scanner(System.in);
		byte p= s4.nextByte();
		//salida
		System.out.println("Su division es la "+ "\""+d+"\"");
		System.out.println("La cantidad de goles es "+g);
		System.out.println("La capacidad de la cancha es "+c);
		System.out.println("El promedio de goles es "+p);
		
	}
}