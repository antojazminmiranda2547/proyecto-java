package m�dulo3_y_m�dulo4;

import java.util.Scanner;

public class Ejercicio_15 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		char categor�a;
		System.out.println("Coloque la categor�a de su veh�culo.\nRecuerde que estas van desde la \"a\" hasta la \"c\"");
		System.out.print("�Cu�l es la categor�a de su veh�culo?  ");
		categor�a = scan.next().charAt(0);
		
		switch(categor�a) {
		case 'a': case 'A':
			System.out.println("Su veh�culo tiene: ");
			System.out.println("4 ruedas");
			System.out.println("Un motor");
			break;
		case 'b': case 'B':
			System.out.println("Su veh�culo tiene: ");
			System.out.println("4 ruedas");
			System.out.println("Un motor");
			System.out.println("Cerradura centralizada");
			System.out.println("Aire acondicionado");
			break;
		case 'c': case 'C':
			System.out.println("Su veh�culo tiene: ");
			System.out.println("4 ruedas");
			System.out.println("Un motor");
			System.out.println("Cerradura centralizada");
			System.out.println("Aire acondicionado");
			System.out.println("AirBag");
			break;
		default:
			System.out.println("La clase no es v�lida");
			break;
		}
		
		scan.close();

	}

}
