package módulo3_y_módulo4;

import java.util.Scanner;

public class Ejercicio_9 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println(" Piedra (0) Papel (1) Tijeras (2) ");
		System.out.print("Primer jugador, elija  ");
		int jug1 = scan.nextInt();
		System.out.print("Segundo jugador, elija  ");
		int jug2 = scan.nextInt();
		if(jug1==0 && jug2==0) {System.out.println("Empate");}
		else if(jug1==1 && jug2==1) {System.out.println("Empate");}
		else if(jug1==2 && jug2==2) {System.out.println("Empate");}
		else if(jug1==2 && jug2==1) {System.out.println("Primer jugador gana");}
		else if(jug1==0 && jug2==2) {System.out.println("Primer jugador gana");}
		else if(jug1==1 && jug2==0) {System.out.println("Primer jugador gana");}
		else if(jug1==1 && jug2==2) {System.out.println("Segundo jugador gana");}
		else if(jug1==2 && jug2==0) {System.out.println("Segundo jugador gana");}
		else if(jug1==0 && jug2==1) {System.out.println("Segundo jugador gana");}
		scan.close();
	}

}
