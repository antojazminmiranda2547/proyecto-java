package módulo3_y_módulo4;

import java.util.Scanner;

public class Ejercicio_8 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int jug1, jug2;
		
		System.out.println(" Piedra (0) Papel (1) Tijeras (2) ");
		
		System.out.print("Primer jugador, elija   ");
		jug1 = scan.nextInt();
		System.out.print("Segundo jugador, elija  ");
		jug2 = scan.nextInt();
		
		if(jug2==0) {
			if(jug1==1) {System.out.println("Primer jugador gana");}
			else if(jug1==2) {System.out.println("Segundo jugador gana");}
			else {System.out.println("Empate");}
		}
		else if(jug2==1) {
			if(jug1==0) {System.out.println("Segundo jugador gana");}
			else if(jug1==2) {System.out.println("Primer jugador gana");}
			else {System.out.println("Empate");}
		}
		else if(jug2==2) {
			if(jug1==0) {System.out.println("Primer jugador gana");}
			else if(jug1==1) {System.out.println("Segundo jugador gana");}
			else {System.out.println("Empate");}
		}
		scan.close();
	}

}
