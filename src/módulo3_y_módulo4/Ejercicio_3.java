package m�dulo3_y_m�dulo4;

import java.util.Scanner;

public class Ejercicio_3 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Ingres� un mes -----> ");
		String mes = scan.nextLine();
		
		if(mes.equals("enero")) {System.out.println("\""+mes+"\" tiene 31 d�as");}
		else if(mes.equals("marzo")) {System.out.println("\""+mes+"\" tiene 31 d�as");}
		else if(mes.equals("mayo")) {System.out.println("\""+mes+"\" tiene 31 d�as");}
		else if(mes.equals("julio")) {System.out.println("\""+mes+"\" tiene 31 d�as");}
		else if(mes.equals("agosto")) {System.out.println("\""+mes+"\" tiene 31 d�as");}
		else if(mes.equals("octubre")) {System.out.println("\""+mes+"\" tiene 31 d�as");}
		else if(mes.equals("diciembre")) {System.out.println("\""+mes+"\" tiene 31 d�as");}
		else if(mes.equals("febrero")) {System.out.println("\""+mes+"\" tiene 28 d�as");}
		else if(mes.equals("abril")) {System.out.println("\""+mes+"\" tiene 30 d�as");}
		else if(mes.equals("junio")) {System.out.println("\""+mes+"\" tiene 30 d�as");}
		else if(mes.equals("septiembre")) {System.out.println("\""+mes+"\" tiene 30 d�as");}
		else if(mes.equals("noviembre")) {System.out.println("\""+mes+"\" tiene 30 d�as");}
		else {System.out.println("mes incorrecto");}
		
		scan.close();
	}

}
