package m�dulo3_y_m�dulo4;

import java.util.Scanner;

public class Ejercicio_7 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		int N1, N2, N3;
		System.out.print("Primer n�mero  ");
		N1 = scan.nextInt();
		System.out.print("Segundo n�mero  ");
		N2 = scan.nextInt();
		System.out.print("Tercer n�mero  ");
		N3 = scan.nextInt();
		
		if(N1==N2) {
			if(N3>N1) {System.out.println("El mayor: el tercer n�mero");}
			else if(N3<N1){System.out.println("Los mayores: el primer y segundo n�meros");}
			else {System.out.println("Todos los n�meros son iguales");}
		}
		else if(N2==N3) {
			if(N1>N2) {System.out.println("El mayor: el primer n�mero");}
			else if(N1<N2) {System.out.println("Los mayores: el segundo y tercer n�mero");}
			else {System.out.println("Los tres n�meros son iguales");}
		}
		else if(N1==N3) {
			if(N2>N1) {System.out.println("El mayor: el segundo n�mero");}
			else if(N2<N1) {System.out.println("Los mayores: el primer y tercer n�mero");}
			else {System.out.println("Los tres n�meros son iguales");}
		}
		else if(N1>N2) {
			if(N2>N3) {System.out.println("El mayor: el primer n�mero");}
			else if(N3>N2) {
				if(N3>N1) {System.out.println("El mayor: el tercer n�mero");}
				else {System.out.println("El mayor: el primer n�mero");}
			}
		}
		else if(N1>N3) {
			if(N3>N2) {System.out.println("El mayor: el primer n�mero");}
			else if(N2>N3) {
				if(N2>N1) {System.out.println("El mayor: el segundo n�mero");}
				else {System.out.println("El mayor: el primer n�mero");}
			}
		}
		else if(N2>N1) {
			if(N1>N3) {System.out.println("El mayor: el segundo n�mero");}
			else if(N3>N1) {
				if(N3>N2) {System.out.println("El mayor: el tercer n�mero");}
				else {System.out.println("El mayor: el segundo n�mero");}
			}
		}
		else if(N3>N2) {
			if(N2>N1) {System.out.println("El mayor: el tercer n�mero");}
			else if(N1>N2) {
				if(N1>N3) {System.out.println("El mayor: el primer n�mero");}
				else {System.out.println("El mayor: el tercer n�mero");}
			}
		}
		scan.close();
	}
}
