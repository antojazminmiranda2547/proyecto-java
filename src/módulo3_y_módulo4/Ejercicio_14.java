package m�dulo3_y_m�dulo4;

import java.util.Scanner;

public class Ejercicio_14 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
				
		System.out.print("�En que puesto sali� usted? (Expr�selo en palabras)  ");
		String place = scan.nextLine();
		
		switch(place) {
		case "primero": case "Primero":
			System.out.println("Usted tiene la medalla de oro");
			break;
		case "segundo": case "Segundo":
			System.out.println("Usted tiene la medalla de plata");
			break;
		case "tercero": case "Tercero":
			System.out.println("Usted tiene la medalla de bronce");
			break;
		default:
			System.out.println("Siga participando");
		}
		scan.close();
	}
}
