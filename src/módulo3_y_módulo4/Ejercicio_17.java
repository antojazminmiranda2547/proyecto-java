package m�dulo3_y_m�dulo4;

import java.util.Scanner;

public class Ejercicio_17 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("�Alg�n n�mero del que quieras saber su tabla?  ");
		int num = scan.nextInt();
		int cont=1;
		int suma = 0;
		int actual;
		
		while (cont<11) {
			System.out.println(num+"*"+cont+"="+(num*cont));
			actual=num*cont;
			if (actual%2==0) {suma=suma+num*cont;}
			cont++;
		}
		System.out.println("Resultado de los valores pares sumados  "+suma);
		scan.close();
	}
}
