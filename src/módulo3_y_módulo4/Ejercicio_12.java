package m�dulo3_y_m�dulo4;

import java.util.Scanner;

public class Ejercicio_12 {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("El n�mero que ingrese designar� la docena en la que se encuentra");
		System.out.println("Ingrese un n�mero  ");
		int num = scan.nextInt();
		
		if(num>=1 && num<=12) {System.out.println("Su n�mero se encuentra en la primer docena");}
		else if(num>=13 && num<=24) {System.out.println("Su n�mero se encuentra en la segunda docena");}
		else if(num>=25 && num<=36) {System.out.println("Su n�mero se encuentra en la tercer docena");}
		else if(num<1 || num>36) {System.out.println("Su n�mero se encuentra por debajo del l�mite de la primer \ndocena o por encima del l�mite de la segunda");}
		scan.close();
	}
}
